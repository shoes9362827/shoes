<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->text('img')->default('https://vipha.co/wp-content/themes/vipha/images/empty-img.png');
            $table->smallInteger('sale')->default(0);
            $table->double('price');
            $table->timestamps();
        });
        Schema::create('sales',function(Blueprint $table){
            $table->id();
            $table->double("percent_discounts");
            $table->unsignedBigInteger("prd_id");
            $table->foreign("prd_id")->references("id")->on("products")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
