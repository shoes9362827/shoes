<?php

namespace App\Models;


use App\Traits\HandleImageTrait;
use App\Traits\Imaginable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    use HasFactory;

    const IMAGE_SAVE_PATH = 'public/upload/';
    const IMAGE_SHOW_PATH = 'storage/upload/';


    protected $fillable = [
        'name',
        'description',
        'sale',
        'price'
    ] ;

    public function details(){
        return $this->hasMany(ProductDetail::class, "product_id");
    }
    public function images(){
        // return Image::where("imageable_id",$this->id)->where("imageable_type","products")->get();
        return $this->hasMany(Image::class, "imageable_id", "id")->where("imageable_type", "products");
    }
    public function categories(){
        return $this->belongsToMany(Category::class);
    }
    
    public function getBy($dataSearch, $category_id)
    {
        return $this->whereHas('categories', fn($q) => $q->where('category_id', $category_id))->paginate(10);
    }

    public function getImagePathAttribute()
    {
        return asset($this->images->count() > 0 ? 'upload/' . $this->images->first()->url: 'upload/default.png');
    }
    public function salePrice() : Attribute
    {
        return Attribute::make(
            get: fn() => $this->attributes['sale']
                ? $this->attributes['price'] - ($this->attributes['sale'] * 0.01  * $this->attributes['price'])
                : 0
        );
    }


}