<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Coupons\CreateCouponRequest;
use App\Http\Requests\Coupons\UpdateCouponRequest;
use Illuminate\Http\Request;
use App\Models\Coupon;
use Carbon\Carbon;



class CouponController extends Controller
{
    protected $coupon;
     public function __construct(Coupon $coupon)
    {
        $this -> coupon = $coupon;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = $this->coupon->latest('id')->paginate(5);

        return view('admin.coupon.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCouponRequest $request)
    {
        $dataCreate =  $request->all();

        $this->coupon->create($dataCreate);


        return redirect()->route('coupon.index')->with(['message' => 'Create coupon success']);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = $this->coupon->findOrFail($id);

        return view('admin.coupon.edit', compact('coupon'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $coupon = $this->coupon->findOrFail($id);

        $dataUpdate = $request->all();
        $coupon->update($dataUpdate);

        return redirect()->route('coupon.index')->with(['message' => 'Update coupon success']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

            $coupon = $this->coupon->findOrFail($id);
            $coupon->delete();
    
            return redirect()->route('coupon.index')->with(['message' => 'Delete '.$coupon->name.' success']);
    
    }
}
