
const countdown = function(_config) {
  const tarDate = _config.target.dataset.date.split('-');
  const day = parseInt(tarDate[0]);
  const month = parseInt(tarDate[1]);
  const year = parseInt(tarDate[2]);
  let tarTime = _config.target.dataset.time;
  let tarhour, tarmin;

  const $day = _config.target.querySelector('.day .countdown-num')
  const $hour = _config.target.querySelector('.hour .countdown-num')
  const $min = _config.target.querySelector('.min .countdown-num')
  const $sec = _config.target.querySelector('.sec .countdown-num')

  if (tarTime != null) {
    tarTime = tarTime.split(':');
    tarhour = parseInt(tarTime[0]);
    tarmin = parseInt(tarTime[1]);
  }

  // Set the date we're counting down to
  const countDownDate = new Date(year, month-1, day, tarhour, tarmin, 0, 0).getTime();

  const updateTime = () => {
    // Get todays date and time
    const now = new Date().getTime();

    // Find the distance between now an the count down date
    const distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    requestAnimationFrame(updateTime);

    if ($day) $day.innerHTML = addZero(days);
    if ($hour) $hour.innerHTML = addZero(hours);
    if ($min) $min.innerHTML = addZero(minutes);
    if ($sec) $sec.innerHTML = addZero(seconds);

    // If the count down is over, write some text
    if (distance < 0) {
      _config.target.innerHTML = "";
    }
  }

  updateTime();
}

const addZero = (x) => (x < 10 && x >= 0) ? "0"+x : x;

$(function(){
  getTotalValue();

  function getTotalValue() {
      let total = $(".total-price").data("price");
      let couponPrice = $(".coupon-div")?.data("price") ?? 0;
      $(".total-price-all").text(`$${total - couponPrice}`);
  }

  $(document).on("click", ".btn-remove-product", function (e) {
      let url = $(this).data("action");
      confirmDelete()
          .then(function () {
              $.post(url, (res) => {
                  let cart = res.cart;
                  let cartProductId = res.product_cart_id;
                  $("#productCountCart").text(cart.product_count);
                  $(".total-price")
                      .text(`$${cart.total_price}`)
                      .data("price", cart.product_count);
                  $(`#row-${cartProductId}`).remove();
                  getTotalValue();
              });
          })
          .catch(function () {});
  });

  const TIME_TO_UPDATE = 1000;

  $(document).on(
      "click",
      ".btn-update-quantity",
      _.debounce(function (e) {
          let url = $(this).data("action");
          let id = $(this).data("id");
          let data = {
              product_quantity: $(`#productQuantityInput-${id}`).val(),
          };
          $.post(url, data, (res) => {
              let cartProductId = res.product_cart_id;
              let cart = res.cart;
              $("#productCountCart").text(cart.product_count);
              if (res.remove_product) {
                  $(`#row-${cartProductId}`).remove();
              } else {
                  $(`#cartProductPrice${cartProductId}`).html(
                      `$${res.cart_product_price}`
                  );
              }
              getTotalValue();
              cartProductPrice;
              $(".total-price").text(`$${cart.total_price}`);
              Swal.fire({
                  position: "top-end",
                  icon: "success",
                  title: "success",
                  showConfirmButton: false,
                  timer: 1500,
              });
          });
      }, TIME_TO_UPDATE)
    );
}
)