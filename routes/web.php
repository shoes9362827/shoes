<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Client\HomeController;
use App\Http\Controllers\Client\OrderController;
use App\Http\Controllers\Client\ProductController as ClientProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CouponController;
use App\Http\Controllers\Client\CartController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/product', [HomeController::class, 'products'])->name('productlist');

Route::get('/index', function () {
    return view('client.layout.app');
});

Route::get('/blog', function () {
    return view('client.blog.blog');
})->name('blog');

Route::get('/myaccount', function () {
    return view('client.myaccount.dashboard');
})->name('myaccount');

Route::get('/client_account', function () {
    return view('client.myaccount.account');
})->name('myaccount');

Route::get('/client_address', function () {
    return view('client.myaccount.addresses');
})->name('myaccount');

Route::get('/client_wishlist', function () {
    return view('client.myaccount.wishlist');
})->name('myaccount');

Route::get('/client_orders', function () {
    return view('client.myaccount.orders');
})->name('myaccount');

Route::get('/about', function () {
    return view('client.about.about');
})->name('about');

Route::get('/contact', function () {
    return view('client.contact.contact');
})->name('contact');

Route::get('/productdetail', function () {
    return view('client.product.productdetail');
})->name('productdetail');

Route::get('/myaccount/orders', [MyAccountController::class, 'orders'])->name('myaccount.orders');

Route::prefix('admin')->group(function () {
    Route::get('/categories', [CategoryController::class, 'index'])->name('admin.categories.index');
    Route::get('/categories/create', [CategoryController::class, 'create'])->name('admin.categories.create');
    Route::post('/categories', [CategoryController::class, 'store'])->name('admin.categories.store');
});

Route::get('product/{category_id}', [ClientProductController::class, 'index']) -> name('client.product.productlist');

Route::get('productdetail/{id}', [ClientProductController::class, 'show']) -> name('client.product.productdetail');


//admin route
Route::middleware('auth')->group(function(){
 
    Route::get('/dashboard', function () {
        return view('admin.dashboard.index');
    })->name('dashboard');

    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('products', ProductController::class);
    Route::resource('coupon', CouponController::class);
    Route::post('/admin/products', [AdminController::class, 'products'])->name('admin.products');

});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/search', [ClientProductController::class, 'search'])->name('client.product.search');

Route::middleware('auth')->group(function(){
    Route::post('add-to-cart', [CartController::class, 'store']) -> name('client.cart.add');
    Route::get('cart', [CartController::class, 'index']) -> name('client.cart.cart');
    Route::post('update-quantity-product-in-cart/{cart_product_id}', [CartController::class, 'updateQuantityProduct'])->name('client.cart.update_product_quantity');

    Route::post('apply-coupon', [CartController::class, 'applyCoupon']) ->name('client.cart.apply_coupon');
    Route::get('checkout', [CartController::class, 'checkout']) -> name('client.checkout.index')->middleware('user.can_checkout_cart');
    Route::post('process-checkout', [CartController::class, 'processCheckout'])->name('client.checkout.proccess')->middleware('user.can_checkout_cart');

});